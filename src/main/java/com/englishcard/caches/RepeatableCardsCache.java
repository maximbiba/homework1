package main.java.com.englishcard.caches;

import java.util.ArrayList;
import java.util.List;

public class RepeatableCardsCache {
    private List<Integer>  repeatableCards;

    public RepeatableCardsCache() {
        this.repeatableCards = new ArrayList<>();
    }


    public void addRepeatableNum(int repeatableNum){
        repeatableCards.add(repeatableNum);
    }
    public boolean checkNumsAtCache(int repeatableNum){

        return repeatableCards.contains(repeatableNum);

    }


    public int getSize() {
        return repeatableCards.size()
    }

    public void cleanRepeatableCards(){
        repeatableCards.clear();

    }

}
