package main.java.com.englishcard.caches;

import main.java.com.englishcard.models.Card;

public class CardsCache {
    private Card[] cards;

    public CardsCache(Card[] cards) {
        this.cards = cards;
    }

    public CardsCache() {

    }

    public Card getCard(int numberofCard){
        return cards[numberofCard];

    }
}
