package main.java.com.englishcard.services;
import main.java.com.englishcard.caches.CardsCache;
import main.java.com.englishcard.caches.RepeatableCardsCache;
import main.java.com.englishcard.models.Card;
import main.java.com.englishcard.utils.RandomNum;

import java.util.Random;
import java.util.Scanner;

public class CardsTranslatorService {
    private static final String MSG_WELCOME = "Welcome to the game English Cards";
    private static final String MSG_GAME_QUESTION = "Do you want to start game ? \n enter yes if you want or any button if you not";
    private static final String RESPONSE_Y = "y";
    private static final String MSG_ENGLISH_WORD = "";
    private static final int NUMBERS = 10;


    private CardsCache cardsCache;
    private Scanner scanner;
    private RandomNum randomNumService;
    private RepeatableCardsCache repeatableCardsCache;



    public void startGame() {
        System.out.println(MSG_WELCOME);
        while (true) {
            System.out.println(MSG_GAME_QUESTION);
            String responseFromUser = scanner.next();
            if (!responseFromUser.equalsIgnoreCase(RESPONSE_Y)) {
                break;

            }
            playTenCardGame();
        }
    }

    private int getRandomNum() {
        while (true) {
            int randomNum = randomNumService.getRandomNumber();
            if (repeatableCardsCache.checkNumsAtCache(randomNum)) {
                repeatableCardsCache.addRepeatableNum(randomNum);
                return randomNum;
            }
        }
    }

    private String getUserWord(int randomWord) {
        Card card = cardsCache.getCard(randomWord);
        System.out.println(MSG_ENGLISH_WORD + card.getEnglishWord());
        return scanner.nextLine();

    }

    private void playTenCardGame() {
        for (int times = 0; times < NUMBERS; times++) {
            int randomNum = getRandomNum();
            if (repeatableCardsCache.getSize() == 10) {
                repeatableCardsCache.cleanRepeatableCards();
            }
            Card card = cardsCache.getCard(randomNum);
            String usersWord = getUserWord(randomNum, card.getEnglishWord());
            if (usersWord.equalsIgnoreCase(card.getRussianWord())) {
                System.out.println("yes correct");

            }
        }
    }
}
