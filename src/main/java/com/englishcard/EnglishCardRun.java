package main.java.com.englishcard;

import main.java.com.englishcard.caches.CardsCache;
import main.java.com.englishcard.caches.RepeatableCardsCache;
import main.java.com.englishcard.models.Card;
import main.java.com.englishcard.services.CardsTranslatorService;
import main.java.com.englishcard.utils.RandomNum;

import java.util.Random;
import java.util.Scanner;

public class EnglishCardRun {
    public static void main(String[] args) {
        Card card1 = new Card("car", "машина");
        Card card2 = new Card("map", "карта");
        Card card3 = new Card("travel", "путешествия");
        Card card4 = new Card("bread", "хлеб");
        Card card5 = new Card("voice", "голос");
        Card[] cards = {card1, card2, card3, card4, card5};

        CardsCache cardsCache = new CardsCache();
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        RandomNum randomNum = new RandomNum();
        RepeatableCardsCache repeatableCardsCache = new RepeatableCardsCache();


        new CardsTranslatorService(cardsCache, scanner, randomNum, repeatableCardsCache).startGame();
    }
}
